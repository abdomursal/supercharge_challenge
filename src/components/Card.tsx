import React from "react";
import { Colors } from "../constants/Colors";
import { Image, StyleSheet, Text, TouchableOpacity } from "react-native";
import { CardProps } from "../types/CardProps";
import { imageBaseUrl } from "../constants/globals";


const Card = ({poster,title,overview, id, navigation}:CardProps) => {
  return (
    <TouchableOpacity style={styles.container} onPress={()=> navigation.navigate('Details', {data:{poster,title,overview,id}})}>
          <Image
            style={styles.image}
            source={{uri:`${imageBaseUrl}${poster}`}}
            />
        <Text style={styles.movieTitle}>{title}</Text>
        <Text style={styles.movieOverview}>{overview}</Text>
    </TouchableOpacity>
  );
};

export default Card;

const styles = StyleSheet.create({
  container: {
    width:'85%', 
    backgroundColor: Colors.white,
    marginVertical:30,
    height:400,
    paddingTop:10,
    alignSelf:'center',
    borderRadius:30,
    shadowOpacity: 0.3,
    shadowOffset: {
      height: 10,
      width: 0,

    },
    shadowColor:Colors.shadow,
    shadowRadius: 20,

  
  },
  image: {
    height: 230,
    width: "100%",
    borderTopLeftRadius:30,
    borderTopRightRadius:30,
    resizeMode: "contain",
    marginBottom:10,
  },
  movieTitleWrap:{


  },
  movieTitle:{
    fontSize:24,
    textAlign:'center',
  },
  movieOverview:{
    padding:10,
    maxHeight:100,
  }
  
});
