import React, { useState } from 'react'
import { StyleSheet, TextInput, KeyboardAvoidingView, Image, TouchableOpacity, Alert } from 'react-native'
import { Colors } from '../constants/Colors'
import { searchMovie } from '../lib/searchMovie'
import { useAppDispatch } from '../store/hooks'
import { getMovies } from '../store/moviesReducer'
import { viewportHeight, viewportWidth } from '../utils'


const SearchBox = () => {
  const dispatch=useAppDispatch()
  const [value, setValue] = useState('')
  const searchText = async ()=>{
    if (value ===''){
    return  Alert.alert('Please, Enter search keyword ')
    }
    const data = await searchMovie(value)
    dispatch(getMovies(data.results))
  }
  return (
      <KeyboardAvoidingView style={styles.container}>
          <TextInput  style={styles.input} placeholder='Search' onChangeText={setValue}/>
          <TouchableOpacity style={styles.searchButton} onPress={searchText}>
          <Image style={styles.icon} source={require('../../assets/search.png')}/>
          </TouchableOpacity>
      </KeyboardAvoidingView>

  )
}
const styles = StyleSheet.create({
    container:{
       height:viewportWidth/8,
       width:viewportWidth/1.44,
       borderRadius:viewportHeight/2,
       backgroundColor:Colors.white,
       justifyContent:'space-between',
       alignItems:'center',
       alignSelf:'center',
       borderWidth:0,
       flexDirection:'row',
       marginTop:20,
       marginBottom:10,
       paddingLeft:10,
    },
    input:{
        paddingLeft:10,
        width:'80%',
        height:'90%',
        color:Colors.black,
        fontSize:15,
        fontWeight:'600',
    },
    icon:{
      height:20,
      width:20,
    },
    searchButton:{
      backgroundColor:Colors.secondary,
      height:40,
      width:40,
      borderRadius:25,
      alignItems:'center',
      justifyContent:'center',
      marginRight:10,
    }
})

export default SearchBox