import {FlatList, SafeAreaView, StyleSheet, Text, View} from 'react-native';
import React from 'react';
import Card from './Card';
import {RootStackScreenProps} from '../types/NavigationTypes';
import {useAppSelector} from '../store/hooks';

const MovieList = ({navigation}: RootStackScreenProps<any>) => {
  const data = useAppSelector(state => state.movies.movies);

  if (data.length === 0) {
    return (
      <View style={styles.notFound}>
        <Text style={styles.notFoundText}>Query Not Found</Text>
        <Text style={styles.notFoundText}>Please, enter a new query</Text>
      </View>
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={data}
        renderItem={({item}) => (
          <Card
            poster={item.poster_path}
            overview={item.overview}
            title={item.title}
            id={item.id}
            navigation={navigation}
          />
        )}
      />
    </SafeAreaView>
  );
};

export default MovieList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  notFound: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  notFoundText: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});
