import { createSlice } from "@reduxjs/toolkit";

export type movieEntitesProps={
    adult: boolean
    backdrop_path: string
    genre_ids: any
    id: number
    original_language: string
    original_title: string
    overview: string
    popularity: number
    poster_path: string
    release_date: string
    title: string
    video: false
    vote_average: number
    vote_count: number
}


export type MovieProps={
    movies:Array<movieEntitesProps>
}

export const initialState: MovieProps = {
   movies:[]
}

export const MoviesSlice = createSlice({
  name: "store",
  initialState,
  reducers: {
    getMovies: (state , action) => {
      return { ...state, movies:action.payload};
    },
  },
});

export const { getMovies } = MoviesSlice.actions;

export default MoviesSlice.reducer;
