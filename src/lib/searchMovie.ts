import { apiKey } from "../constants/globals";
import { API_BASE_URL, httpClient, onApiError } from "./api";


export const searchMovie = async (query:string) => {
  const URL = API_BASE_URL + `search/movie`;  
  const params = {
    "api_key": apiKey,
    query
}
  return httpClient
    .get(URL, {params:params})
    .then((response: Record<string, any>) => { 
      return response.data;
    })
    .catch((err: any) => {
      onApiError(err);
    });
};

