import { apiKey } from "../constants/globals";
import { API_BASE_URL, httpClient, onApiError } from "./api";


export const getDetailsMovie = async (id:number) => {
  const URL = API_BASE_URL + `movie/`+ id;  
  const params = {
    "api_key": apiKey,
}

  return httpClient
    .get(URL, {params:params})
    .then((response: Record<string, any>) => { 
      return response.data;
    })
    .catch((err: any) => {
      onApiError(err);
    });
};

