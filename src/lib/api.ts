import axios from 'axios'
import { Alert } from 'react-native';

export const API_BASE_URL = 'https://api.themoviedb.org/3/'

export const HEADERS = {
  "Content-Type": "application/json",
  "Accept": "application/json",
}


export const httpClient = axios.create({
  baseURL: API_BASE_URL,
  headers:HEADERS,
})


export const onApiError = (err:any) => {
  if (!err || !err.response) {
    Alert.alert(`api error her with error ${err.response.status}`)
  }
  Alert.alert('Please enter correct information')
}
