import { viewportHeight,viewportWidth} from './Dimensions'

export {
    viewportHeight,
    viewportWidth
}