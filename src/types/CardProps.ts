import { NavigationProp } from "@react-navigation/native";

export type CardProps ={
    poster:string;
    id:number;
    title:string;
    overview:string;
    navigation:NavigationProp<any,any>
   
} 