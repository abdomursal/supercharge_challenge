import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import Home from '../screens/Home';
import Details from '../screens/Details';
import { RootStackParamList } from '../types/NavigationTypes';

const Stack = createNativeStackNavigator<RootStackParamList>();


const Navigation = () => {
  return (
    <NavigationContainer>
    <RootNavigator/>
  </NavigationContainer>
  )
}

function RootNavigator() {
  return (
    <Stack.Navigator initialRouteName='Home'>
      <Stack.Screen name="Home" component={Home} options={{ title: 'Home' }} />
      <Stack.Screen name="Details" component={Details} options={{ title: 'Details' }} />
    </Stack.Navigator>
  );
}

export default Navigation