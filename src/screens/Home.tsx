import React from 'react';
import {StyleSheet, View} from 'react-native';
import SearchBox from '../components/SearchBox';
import {Colors} from '../constants/Colors';
import MovieList from '../components/MovieList';
import {RootStackScreenProps} from '../types/NavigationTypes';

const Home = ({navigation, route}: RootStackScreenProps<any>) => {
  return (
    <View style={styles.container}>
      <SearchBox />
      <MovieList navigation={navigation} route={route} />
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.primary,
    flex: 1,
  },
});
