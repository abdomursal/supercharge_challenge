import React, { useEffect, useState } from 'react'
import { Image, ScrollView, StyleSheet, Text, View } from 'react-native'
import { Colors } from '../constants/Colors'
import { imageBaseUrl } from '../constants/globals'
import { getDetailsMovie } from '../lib/details'
import { RootStackScreenProps } from '../types/NavigationTypes'

const Details = ({route}:RootStackScreenProps<any>) => {
  const [budget, setBudget] = useState('')
  const {title, poster, overview, id} = route.params?.data
  
  const  getDetails = async ()=>{
    if(id !== undefined ){
      const details = await getDetailsMovie(id)
      setBudget(details.budget)
    }
    
  }
  useEffect(() => {
    getDetails()
  }, [])
  
  return (
    <ScrollView style={styles.container}>
      <Image  style={styles.image} source={{uri:`${imageBaseUrl}${poster}`}}/>
      <View style={styles.textWrap}>
      <Text style={[styles.text, {textAlign:'center', marginVertical:10}]}>{title} </Text>
      <Text style={styles.text}>Budget : {budget}</Text>
      <Text style={styles.text}>Overview : </Text>
      <Text style={styles.textOverview}>{overview}</Text>
      </View>
    </ScrollView>
  )
}

export default Details

const styles = StyleSheet.create({
  container:{
    // flex:1,
    backgroundColor:Colors.primary
  },
  image:{
    height:450,
    marginTop:10,
    width:'100%',
   resizeMode:'contain',
   shadowColor:'black',
   shadowOpacity:.5,
   shadowOffset:{
     height:10,
     width:0,
   },
   shadowRadius:10

  },
  textWrap:{
    padding:15,
    marginVertical:10
  },
  text:{
    fontSize:18,
    fontWeight:'600',
    color:Colors.black,
    paddingBottom:20,
  },
  textOverview:{
    fontSize:16
  }
})