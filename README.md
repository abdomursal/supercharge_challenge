# superCharge_challenge

The task is to design and develop a React Native application in 3 hours, which user can displays and search movies with movies's details from the api provided


# To get start it

- clone the repo to your local 

- open the command line in the root directory and run:
`yarn install`

- go to ios folder `cd ios`

- run in ios directory `pod install`


# screenshot

<img src='./assets/home_screenshot.png' width="300">

