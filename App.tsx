
import React from 'react';
import Navigation from './src/navigation/Navigation';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider } from 'react-redux'
import  {store}  from './src/store/store'

const App = () => {
  return (
    <Provider store={store}>
      <Navigation/>
    </Provider>
  );
};


export default App;
